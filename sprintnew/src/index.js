import React from "react";
import ReactDOM from "react-dom";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import { logout } from "./services/auth";
import Login from "./components/Login/Login";
import Tasks from "./components/tasks/Tasks";
import EditTask from "./components/tasks/EditTask";
import { HashRouter as Router, Link, Route, Routes } from "react-router-dom";
import { Navbar, Nav, Button, Container } from "react-bootstrap";

class App extends React.Component {
    render() {
        return(
            <div>
                <Router>
                    <Navbar bg="dark" variant="dark" expand>
                        <Navbar.Brand as={Link} to="/">
                            Sprint
                        </Navbar.Brand>
                        <Nav class="mr-auto">
                            <Nav.Link as={Link} to="/tasks">
                                Tasks
                            </Nav.Link>
                        </Nav>

                        {window.localStorage['jwt']?
                            <Button onClick= {() => logout()}>Log out</Button> :
                            <Nav.Link as ={Link} to="/login">Log in</Nav.Link>
                        }
                    </Navbar>
                    <Container>
                        <Routes>
                            <Route path="/" element={<Home/>}/>
                            <Route path="/tasks" element={<Tasks/>}/>
                            <Route path="/tasks/edit/:id" element={<EditTask/>}/>
                            <Route path="/login" element={<Login/>}/>
                            <Route path="*" element={<NotFound/>}/>
                        </Routes>
                    </Container>
                </Router>
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.querySelector('#root'));