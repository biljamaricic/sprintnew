import React from "react";
import { Button, Form } from "react-bootstrap";
import SprintsAxios from "../../apis/SprintsAxios";
import SprintsAxiosss from "../../apis/SprintsAxios";
import { withNavigation, withParams } from "../../routeconf";


class EditTask extends React.Component{
    constructor(props) {
        super(props);

        let task = {
            name: "",
            employee: "",
            points: 0,
            stateId: -1,
            sprintId: -1.
        };

        this.state = {
            task:task,
            sprints: [],
            states: []
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        this.getTasks(0);
        this.getSprints();
        this.getStates();
    }

    getTasks(page) {
        let config = {
            params : {
                pageNo: page
            }
        };

        if(this.state.search.taskName != "") {
            config.params.taskName = this.state.search.taskName;
        }

        if(this.state.search.sprintId != -1) {
            config.params.sprintId = this.state.search.sprintId;
        }

        SprintsAxios.get("/zadaci", config)
        .then(res => {
            console.log(res);
            if(res && res.status === 200) {
                const sprintSum = res.headers["sprint-total"]?res.headers["sprint-total"]:"";
                this.setState({
                    pageNo: page,
                    tasks: res.data,
                    totalPages: res.headers["total-pages"],
                    sprintSum: sprintSum
                });
            }
        })
        .catch(error => {
            console.log(error);
            alert('Error occured while getting tasks, please try again.');
        })

    }

    getSprints() {
        SprintsAxios.get("/sprintovi")
        .then(res => {
            console.log(res);
            this.setState({
                sprints: res.data,
            });
        })
        .catch(error => {
            console.log(error);
            alert('Error occured while getting sprints, please try again.')
        })
    }

    getStates() {
        SprintsAxios.get("/stanja")
        .then(res => {
            console.log(res);
            this.setState({
                states: res.data,
            });
        })
        .catch(error => {
            console.log(error);
            alert('Error occured while getting states, please try again.')
        })
    }

    async doEdit() {
        try{
            await SprintsAxios.put("/zadaci/" + this.props.params.id, this.state.task);
            this.props.navigate("/tasks");
        } catch(error) {
            alert("Error occured, edit not saved. Please try again.")
        }
    }

    valueInputChange(event) {
        let control = event.target;

        let name= control.name;
        let value = control.value;

        let task = this.state.task;
        task[name] = value;

        this.setState({task:task});
    }

    render() {
        return(
            <div>
                <h1>Task</h1>

                <Form>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                        onChange={(event) => this.valueInputChange(event)}
                        name="name"
                        value={this.state.task.name}
                        as="input"
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Employee</Form.Label>
                        <Form.Control
                        onChange={(event) => this.valueInputChange(event)}
                        name="employee"
                        value={this.state.task.employee}
                        as="input"
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Points</Form.Label>
                        <Form.Control
                        onChange={(event) => this.valueInputChange(event)}
                        name="points"
                        value={this.state.task.points}
                        as="input"
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>State</Form.Label>
                        <Form.Control
                        onChange={(event) => this.valueInputChange(event)}
                        name="stateId"
                        value={this.state.task.stateId}
                        as="select"
                        >
                            <option value={-1}></option>
                            {this.state.state.map((state) => {
                                return(
                                    <option value={state.id} key={state.id}>
                                        {state.name}
                                    </option>
                                );
                            })};
                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Sprint</Form.Label>
                        <Form.Control
                        onChange={(event) => this.valueInputChange(event)}
                        name="sprintId"
                        value={this.state.task.sprintId}
                        as="select"
                        >
                            <option value={-1}></option>
                            {this.state.sprints.map((sprint) => {
                                return(
                                    <option value={sprint.id} key={sprint.id}>
                                        {sprint.name}
                                    </option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>
                    <Button variant="primary" onClick={() => this.doEdit}>
                        Edit
                    </Button>
                </Form>
            </div>
        )
    }

}

export default withNavigation(withParams(EditTask));