import React from "react";
import SprintsAxios from "../../apis/SprintsAxios";
import { withNavigation } from "../../routeconf";
import { Form, Button, Collapse, Table, ButtonGroup } from "react-bootstrap";

class Tasks extends React.Component{
    constructor(props) {
        super(props);

        let task = {
            name: "",
            employee: "",
            points: "",
            sprintId: -1,
        };

        this.state = {
            task: task,
            tasks: [],
            sprints: [],
            showSearch: false,
            search: { taskName: "", sprintId: -1},
            pageNo: 0,
            totalPages: 1,
            sprintSum: ""
        };
    }

    componentDidMount() {
        this.getData();
    }
    
    getData() {
        this.getSprints();
        this.getTasks(0);
    }


    getTasks(page) {
        let config = {
            params : {
                pageNo: page
            }
        };

        if(this.state.search.taskName != "") {
            config.params.taskName = this.state.search.taskName;
        }

        if(this.state.search.sprintId != -1) {
            config.params.sprintId = this.state.search.sprintId;
        }

        SprintsAxios.get("/zadaci", config)
        .then(res => {
            console.log(res);
            if(res && res.status === 200) {
                const sprintSum = res.headers["sprint-total"]?res.headers["sprint-total"]:"";
                this.setState({
                    pageNo: page,
                    tasks: res.data,
                    totalPages: res.headers["total-pages"],
                    sprintSum: sprintSum
                });
            }
        })
        .catch(error => {
            console.log(error);
            alert('Error occured while getting tasks, please try again.');
        })

    }

    getSprints() {
        SprintsAxios.get("/sprintovi")
        .then(res => {
            console.log(res);
            this.setState({
                sprints: res.data,
            });
        })
        .catch(error => {
            console.log(error);
            alert('Error occured while getting sprints, please try again.')
        })
    }

    goToEdit(taskId) {
        this.props.navigate("/tasks/edit/" + taskId);
    }

    doAdd() {
        var params = {
            name: this.state.name,
            employee: this.state.employee,
            points: this.state.points,
            sprintId: this.state.sprintId,
        };
        
        SprintsAxios.post("/zadaci", params)
        .then(res => {
            console.log(res);
            alert('Task successfully added.')
            this.setState({tasks: res.data})
        }) 
        .catch(error => {
            console.log(error);
            alert('Error occured, please try again.')
        })
    }

    async doDelete(taskId) {
        try {
            await SprintsAxios.delete("/zadaci" + taskId);
            var nextPage
            if(this.state.pageNo==this.state.totalPages-1 && this.state.tasks.length==1) {
                nextPage = this.state.pageNo-1
            } else {
                nextPage = this.state.pageNo
            }
            await this.getTasks(nextPage);
        } catch(error) {
            alert("Error occured, please try again.")
        }
    }

    doSearch() {
        this.getTasks(0);
    }

    async changeState(taskId) {
        try{
            const ret = await SprintsAxios.post(`/tasks/${taskId}/change_state`);
            var tasks = this.state.tasks;
                tasks.forEach((element, index) => {
                    if(element.id === taskId) {
                        tasks.splice(index, 1, ret.data);
                        this.setState({tasks:tasks});
                    }
                });
        } catch (error){
            alert("Nije moguće promeniti stanje..")
        }
    }


    addValueInputChange(event) {
        let control = event.target;

        let name= control.name;
        let value = control.value;

        let task = this.state.task;
        task[name] = value;

        this.setState({task:task});
    }

    searchValueInputChange(event) {
        let control = event.target;

        let name= control.name;
        let value = control.value;

        let search = this.state.search;
        search[name] = value;

        this.setState({search:search});
    }

    canCreateTask() {
        const task = this.state.task
        return task.name!= "" &&
            (task.points!="" && task.points>=0 && task.points<=20 && task.points%1==0)
             && task.sprintId != -1
    }

    render() {
        return(
            <div>
                <h1>Tasks</h1>
                {/* {window.localStorage['role']=="ROLE_ADMIN"? */}
                <Form>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                        onChange={(event) => this.addValueInputChange(event)}
                        name = "name"
                        value = {this.state.task.name}
                        as="input"
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Employee</Form.Label>
                        <Form.Control
                        onChange={(event) => this.addValueInputChange(event)}
                        name="employee"
                        value={this.state.task.employee}
                        as="input"
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Points</Form.Label>
                        <Form.Control
                        onChange={(event) => this.addValueInputChange(event)}
                        name="points"
                        value={this.state.task.points}
                        as="input"
                        type="number"
                        min = "0"
                        step ="1"
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Sprint</Form.Label>
                        <Form.Control
                        onChange={(event) => this.addValueInputChange(event)}
                        name="sprintId"
                        value={this.state.task.sprintId}
                        as="select"
                        >
                            <option value={-1}></option>
                            {this.state.sprints.map((sprint) =>{
                                return(
                                    <option value={sprint.id} key={sprint.id}>
                                        {sprint.name}
                                    </option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>
                    <br></br>
                    <Button disabled={!this.canCreateTask} variant="info" onClick={() => this.doAdd()}>
                        Add
                    </Button>
                </Form>
                {/* :null} */}

                <Form.Group style={{marginTop:35}}>
                    <Form.Check type="checkbox" label="Show search form" onClick={(event) => this.setState({showSearch: event.target.checked})}/>
                </Form.Group>
                <Collapse in={this.state.showSearch}>
                <Form style={{marginTop:10}}>
                    <Form.Group>
                        <Form.Label>Task name</Form.Label>
                        <Form.Control
                            value = {this.state.search.taskName}
                            name="taskName"
                            as="input"
                            onChange={(e) => this.searchValueInputChange(e)}
                        ></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Sprint</Form.Label>
                        <Form.Control
                        onChange={(event) => this.searchValueInputChange(event)}
                        name="sprintId"
                        value={this.state.search.sprintId}
                        as="select"
                        >
                            <option value={-1}></option>
                            {this.state.sprints.map((sprint) => {
                                return(

                                    <option value={sprint.id} key={sprint.id}>
                                        {sprint.name}
                                    </option>
                                );
                            })}
                        </Form.Control>
                    </Form.Group>
                    <Button onClick={() => this.doSearch()}>Search</Button>
                </Form>
                </Collapse>

                <ButtonGroup style={{ marginTop: 25, float:"right"}}>
                    <Button 
                        style={{ margin: 3, width: 90}}
                        disabled={this.state.pageNo==0} onClick={()=>this.getTasks(this.state.pageNo-1)}>
                        Previous
                    </Button>
                    <Button
                        style={{ margin: 3, width: 90}}
                        disabled={this.state.pageNo==this.state.totalPages-1} onClick={()=>this.getTasks(this.state.pageNo+1)}>
                        Next
                    </Button>
                </ButtonGroup>

                <Table bordered striped style={{marginTop:5}}>
                    <thead className="thead-dark">
                        <tr>
                            <th>Name</th>
                            <th>Employee</th>
                            <th>Points</th>
                            <th>State</th>
                            <th>Sprint</th>
                            <th colSpan={2}>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.tasks.map((task) => {
                            return(
                                <tr key={task.id}>
                                    <td>{task.name}</td>
                                    <td>{task.employee}</td>
                                    <td>{task.points}</td>
                                    <td>{task.stateName}</td>
                                    <td>{task.sprintName}</td>
                                    <td>
                                        <Button
                                            disabled={task.stateId===3}
                                            variant="info"
                                            style={{marginLeft: 5}}
                                            onClick={() => this.changeState(task.id)}>
                                            Change State
                                        </Button>
                                        {window.localStorage['role']="ROLE_ADMIN"?
                                    [   <Button
                                            variant="warning"
                                            style={{marginLeft: 5}}
                                            onClick={() => this.goToEdit(task.id)}>
                                            Edit    
                                        </Button>,

                                        <Button
                                        variant="danger"
                                        style={{marginLeft: 5}}
                                        onClick={() => this.doDelete(task.id)}>
                                            Delete
                                        </Button>]:null}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
                <h2 hidden={this.state.sprintSum==""}>Suma bodova za sprint je {this.state.sprintSum}</h2>
            </div>
        );
    }
}

export default withNavigation(Tasks);